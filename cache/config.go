package cache

import (
	"creator/log"
	"github.com/joho/godotenv"
	"github.com/redis/go-redis/v9"
	"os"
)

func getClient() *redis.Client {
	err := godotenv.Load()
	if err != nil {
		log.Info("Error loading .env file")
	}
	return redis.NewClient(&redis.Options{
		Addr:     os.Getenv("APP_CACHE_ADDR"),
		Password: os.Getenv("APP_CACHE_PASSWORD"),
		DB:       0,
	})
}

var instance *redis.Client = nil

func GetInstance() *redis.Client {
	if instance == nil {
		instance = getClient()
		return instance
	} else {
		return instance
	}
}
