# Getting Started with Course App 
This is CRUD REST API witch using MySQL DB and Redis Cache (docker-compose)

In this project i'm implement CRUD app "User-Course-Lesson" that manage data on Cashe using Redis and MySQL database.



##  Server will start on 
[http://localhost:8080](http://localhost:8080)


CREATE USER:
#### `http://localhost:8080/createuser/{user}/{user_email}`
REGISTRATION USER ON THE COURSE
#### `http://localhost:8080/user/subscribe/{userID}/{courseID}`
GET USER INFO
#### `http://localhost:8080/user/{userID}/me`
GET LIST OF USERS COURSES
#### `http://localhost:8080/user/{userID}/courses`
UNSUBSCRIBE USER FROM THE COURSE
#### `http://localhost:8080/user/unsubscribe/{userID}/{courseID}`
DELETE USER ACCOUNT
#### `http://localhost:8080/user/delete/{userID}`

CREATE COURSE
#### `http://localhost:8080/course/create/{name}`
DELETE COURSE
#### `http://localhost:8080/course/delete/{courseID}`
RENAME COURSE
#### `http://localhost:8080/course/{courseID}/rename/{newname}`
GET COURSE
#### `http://localhost:8080/course/{courseID}`
GET COURSES
#### `http://localhost:8080/courses`

CREATE LESSON - UPDATE COURSE
requestBody:
{
    "lesson_name": "INTRO",
    "lesson_description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi deserunt quidem unde, laudantium quam sequi iste fugit ipsa. Voluptatem, nostrum."
}
#### `http://localhost:8080/lesson/create/course/{courseID}`

DELETE LESSON
#### `http://localhost:8080/lesson/delete/{lessonID}`