CREATE TABLE lessons (
                         id INT AUTO_INCREMENT PRIMARY KEY,
                         name VARCHAR(50),
                         description VARCHAR(150),
                         name_en VARCHAR(50),
                         description_en VARCHAR(150)
);

CREATE TABLE users (
                       id INT AUTO_INCREMENT PRIMARY KEY,
                       name VARCHAR(50) UNIQUE,
                       email VARCHAR(50) UNIQUE

);

CREATE TABLE courses (
                         id INT AUTO_INCREMENT PRIMARY KEY,
                         name VARCHAR(50) UNIQUE,
                         name_en VARCHAR(50) UNIQUE
);

CREATE TABLE course_lessons (
                                course_id VARCHAR(50) not null,
                                lesson_id VARCHAR(50) not null,
                                PRIMARY KEY (course_id, lesson_id )
);

CREATE TABLE user_courses (
                              user_id VARCHAR(50) not null,
                              course_id VARCHAR(50) not null,
                              PRIMARY KEY (user_id , course_id )
);
