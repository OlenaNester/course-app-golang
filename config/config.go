package config

import (
	"creator/log"
	"github.com/joho/godotenv"
	"os"
)

type ENVStruct struct {
	Email    string
	Password string
}

var instance *ENVStruct

func GetDotEnv() *ENVStruct {
	if instance == nil {
		err := godotenv.Load()
		if err != nil {
			log.Info("Error loading .env file")
			return nil
		}
		instance = &ENVStruct{
			os.Getenv("APP_EMAIL"),
			os.Getenv("APP_EMAIL_PASSWORD"),
		}
		return instance
	} else {
		return instance
	}
}
