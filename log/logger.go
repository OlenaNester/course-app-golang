package log

import (
	"go.uber.org/zap"
)

var logger *respLogger

type respLogger struct {
	//  Open-Closed Principle
	Logger *zap.Logger
}

func getLogger() *respLogger {
	if logger == nil {
		l, _ := zap.NewDevelopment()
		defer l.Sync()
		o := &respLogger{
			Logger: l,
		}
		return o
	} else {
		return logger
	}
}

func (r respLogger) warnLog(msg string) {
	r.Logger.Warn(msg)
}

func Info(msg string) {
	l := getLogger()
	l.warnLog(msg)
}
