package responses

import (
	"creator/log"
	"encoding/json"
	"net/http"
)

func ResponseCreate(obj string, str string, rw http.ResponseWriter) {
	resp := make(map[string]string)
	resp["message"] = obj + ` with name: ` + str + ` was created successfully`
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Info("Error happened in JSON marshal")
	}
	rw.Write(jsonResp)
}

func ResponseAction(obj1 string, obj1N string, obj2 string, obj2N string, act string, rw http.ResponseWriter) {
	resp := make(map[string]string)
	switch {
	case act == "update":
		resp["message"] = obj1 + ` : ` + obj1N + ` was renamed to ` + obj2 + ` : ` + obj2N
	case act == "subscribed":
		resp["message"] = obj1 + ` : ` + obj1N + ` is ` + act + ` successfully to  ` + obj2 + `` + obj2N
	case act == "deleted":
		resp["message"] = obj1 + ` : ` + obj1N + ` was deleted successfully `
	case act == "unsubscribed":
		resp["message"] = obj1 + ` : ` + obj1N + ` is ` + act + ` from ` + obj2 + ` : ` + obj2N
	case act == "append":
		resp["message"] = obj2 + ` : ` + obj2N + ` is ` + act + ` to ` + obj1 + ` : ` + obj1N
	default:
		resp["message"] = obj1 + ` : ` + obj1N + ` is ` + act + ` on ` + obj2 + ` : ` + obj2N
	}
	jsonResp, err := json.Marshal(resp)

	if err != nil {
		log.Info("Error happened in JSON marshal")
	}
	rw.Write(jsonResp)

}

func ResponseError(m string, err error, rw http.ResponseWriter) {
	resp := make(map[string]string)
	resp["message"] = `Error was happened: ` + err.Error() + m
	jsonResp, err := json.Marshal(resp)

	if err != nil {
		log.Info("Error happened in JSON marshal")
	}
	rw.Write(jsonResp)
}
