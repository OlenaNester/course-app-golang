package main

import (
	"creator/controllers"
	"github.com/Alena557miles/webservergo"
)

func main() {
	userC := &controllers.UserController{}
	lessonC := &controllers.LessonController{}
	coursesC := &controllers.CourseController{}
	webservergo.StartServer(userC, lessonC, coursesC)
}
