package course

import (
	"creator/log"
	"encoding/json"
	"net/http"
)

func ResponseCourses(c []*Course, m string, rw http.ResponseWriter) {
	rw.WriteHeader(http.StatusCreated)
	rw.Header().Set("Content-Type", "application/json")
	resp := make(map[string][]*Course)
	resp[m] = c
	jsonResp, err := json.Marshal(resp)

	if err != nil {
		log.Info("Error happened in JSON marshal")
	}
	rw.Write(jsonResp)
}

func ResponseCourse(c *Course, rw http.ResponseWriter) {
	rw.WriteHeader(http.StatusCreated)
	rw.Header().Set("Content-Type", "application/json")
	resp := make(map[string]*Course)
	resp["course"] = &Course{
		ID:      c.ID,
		Name:    c.Name,
		Lessons: c.Lessons,
	}
	jsonResp, err := json.Marshal(resp)

	if err != nil {
		log.Info("Error happened in JSON marshal")
	}
	rw.Write(jsonResp)
}
