package course

import "creator/entities/lesson"

type Course struct {
	ID      int64            `json:"course_id"`
	Name    string           `json:"course_name"`
	Lessons []*lesson.Lesson `json:"course_lessons"`
}
