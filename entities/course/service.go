package course

import (
	"context"
	"creator/cache"
	"creator/databaseSQL"
	"creator/entities/lesson"
	"creator/log"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"strconv"
	"time"
)

// COURSE SERVICES
func Create(name string) (int64, error) {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return 0, err
	}
	result, err1 := db.Exec(`INSERT INTO courses (name) VALUES (?)`, name)
	if err1 != nil {
		log.Info("Failed to create course")
		return 0, err1
	}
	id, err2 := result.LastInsertId()
	if err2 != nil {
		log.Info("Failed to get created course ID")
		return 0, err2
	}
	return id, nil
}

func Delete(cID string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err1 := db.Exec(`DELETE FROM courses WHERE id=?`, cID)
	if err1 != nil {
		log.Info("Failed to delete course by ID")
		return err1
	}
	_, err3 := db.Exec(`DELETE FROM user_courses WHERE courses_id=?`, cID)
	if err3 != nil {
		log.Info("Failed to delete course by ID from user's courses")
		return err3
	}
	err2 := DeleteFromCache(cID)
	if err2 != nil {
		log.Info("Failed to delete course from cache")
		return err2
	}
	return nil
}

func GetByID(cID string) (*Course, error) {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return nil, err
	}
	c := &Course{}
	err = db.QueryRow(`SELECT id, name FROM courses WHERE courses.id = ?`, cID).Scan(&c.ID, &c.Name)
	if err != nil {
		log.Info("Failed to find course by ID")
		return nil, err
	}
	rows, err1 := db.Query(`SELECT lesson_id FROM course_lessons WHERE course_id = ?`, cID)
	if err1 != nil {
		log.Info("Failed to find course's lessons")
		return nil, err1
	}
	var lA []int
	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err != nil {
			log.Info("Failed to scan lessons from DB")
		}
		lA = append(lA, id)
	}
	rows.Close()
	for _, i := range lA {
		l, err3 := lesson.GetByID(strconv.Itoa(i))
		if err3 != nil {
			log.Info("failed to find Lessons in DB")
			return nil, err3
		}
		c.Lessons = append(c.Lessons, l)
	}
	return c, nil
}

func Rename(cID string, name string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err = db.Exec(`UPDATE courses SET name = ? WHERE id = ?`, name, cID)
	if err != nil {
		log.Info("Failed to rename course")
		return err
	}
	err1 := RenameCache(cID, name)
	if err1 != nil {
		return err1
	}
	return nil
}

func Update(cID string, lID string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err = db.Exec(`INSERT INTO course_lessons VALUES (?,?)`, cID, lID)
	if err != nil {
		log.Info("Failed to update course")
		return err
	}
	return nil
}

func GetCourses() ([]*Course, error) {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return nil, err
	}
	rows, err1 := db.Query(`SELECT id FROM courses`)
	if err1 != nil {
		log.Info("Failed to get courses")
		return nil, err1
	}
	var cA []int
	for rows.Next() {
		var c int
		err = rows.Scan(&c)
		if err != nil {
			log.Info("Failed to scan data from DB")
		}
		cA = append(cA, c)
	}
	rows.Close()
	var courses []*Course
	for _, id := range cA {
		c, err2 := GetByID(strconv.Itoa(id))
		if err2 != nil {
			log.Info("failed to get Course from DB")
			return nil, err2
		}
		courses = append(courses, c)
	}
	return courses, nil
}

// cache course
func CreateCourseCache(c *Course) error {
	var ctx = context.Background()
	client := cache.GetInstance()
	bytes, _ := json.Marshal(c)
	err := client.Set(ctx, `course:`+strconv.FormatInt(c.ID, 10), bytes, 120*time.Second).Err()
	if err != nil {
		return err
	}
	return nil
}

func DeleteFromCache(cID string) error {
	var ctx = context.Background()
	client := cache.GetInstance()
	_, err := client.Get(ctx, `course:`+cID).Result()
	if err == redis.Nil {
		return nil
	} else {
		err := client.Del(ctx, `course:`+cID).Err()
		if err != nil {
			return err
		}
		return nil
	}
}

func GetByIDCache(cID string) *Course {
	var ctx = context.Background()
	client := cache.GetInstance()
	courseStr, err := client.Get(ctx, `course:`+cID).Result()
	if err == redis.Nil {
		return nil
	} else if err != nil {
		log.Info("Failed to get value from cache")
	} else {
		courseToByte := []byte(courseStr)
		var course *Course
		json.Unmarshal(courseToByte, &course)
		return course
	}
	return nil
}

func RenameCache(cID string, name string) error {
	var ctx = context.Background()
	client := cache.GetInstance()
	course := GetByIDCache(cID)
	if course != nil {
		err2 := client.Del(ctx, `course:`+cID).Err()
		if err2 != nil {
			return err2
		}
	}
	course, err1 := GetByID(cID)
	if err1 != nil {
		log.Info(`Failed to find course in DB`)
		return err1
	}
	course.Name = name
	err3 := CreateCourseCache(course)
	if err3 != nil {
		log.Info(`Failed to create course in cache`)
		return err3
	}
	return nil
}

func FindValue(courseID string) (*Course, error) {
	obj := GetByIDCache(courseID)
	if obj != nil {
		return obj, nil
	} else {
		obj, err := GetByID(courseID)
		if err != nil {
			log.Info(`Failed to find artis on DB`)
			return nil, err
		}
		return obj, nil
	}
}

func AppendLessonCache(cID string) error {
	var ctx = context.Background()
	client := cache.GetInstance()
	err := client.Del(ctx, `course:`+cID).Err()
	if err != nil {
		log.Info(`Failed to delete course from cache`)
	}
	c, err1 := GetByID(cID)
	if err1 != nil {
		log.Info(`Failed to find course in DB`)
		return err1
	}
	err3 := CreateCourseCache(c)
	if err3 != nil {
		log.Info(`Failed to create course in cache`)
		return err3
	}
	return nil
}
