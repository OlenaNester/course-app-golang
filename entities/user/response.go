package user

import (
	"creator/log"
	"encoding/json"
	"net/http"
)

func Response(u *User, rw http.ResponseWriter) {
	rw.WriteHeader(http.StatusCreated)
	rw.Header().Set("Content-Type", "application/json")
	resp := make(map[string]*User)
	resp["user"] = &User{
		ID:      u.ID,
		Name:    u.Name,
		Email:   u.Email,
		Courses: u.Courses,
	}
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Info("Error happened in JSON marshal")
	}
	rw.Write(jsonResp)
}
