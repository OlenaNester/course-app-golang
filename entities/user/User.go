package user

import "creator/entities/course"

type User struct {
	ID      int64            `json:"user_id"`
	Name    string           `json:"user_name"`
	Email   string           `json:"user_email"`
	Courses []*course.Course `json:"user_courses"`
}
