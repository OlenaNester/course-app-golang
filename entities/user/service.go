package user

import (
	"creator/databaseSQL"
	"creator/entities/course"
	"creator/log"
	"creator/service/mail"
	"strconv"
)

// USER SERVICES
func CreateUser(name string, email string) (int64, error) {

	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return 0, err
	}
	result, err1 := db.Exec(`INSERT INTO users (name, email) VALUES (?, ?)`, name, email)
	if err1 != nil {
		log.Info("Failed to insert values into users")
		return 0, err1
	}
	id, err3 := result.LastInsertId()
	if err3 != nil {
		log.Info("Failed to get inserted value's ID")
		return 0, err3
	}
	return id, nil
}

func FindUser(uID string) (*User, error) {

	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return nil, err
	}
	u := &User{}
	err = db.QueryRow(`SELECT id, name, email FROM users WHERE users.id = ?`, uID).Scan(&u.ID, &u.Name, &u.Email)
	if err != nil {
		log.Info("Failed to find user")
		return nil, err
	}
	c, err1 := GetUserCourses(uID)
	if err1 != nil {
		log.Info("Failed to get users course")
		return nil, err1
	}
	u.Courses = c
	return u, nil
}

func RegisterUserToCourse(u string, c string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err1 := db.Exec(`INSERT INTO user_courses VALUES (?,?)`, u, c)
	if err != nil {
		log.Info("Failed to register user on course")
		return err1
	}
	user, err2 := FindUser(u)
	if err2 != nil {
		log.Info("Failed to find user")
		return err2
	}
	course, err3 := course.GetByID(c)
	if err3 != nil {
		log.Info("Failed to find user's course by ID")
		return err3
	}
	err4 := mail.SendEmail(course.Name, user.Email)
	if err4 != nil {
		log.Info("Failed to send email to user")
		return err4
	}
	return nil
}

func UnsubscribeUserFromCourse(u string, c string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err = db.Exec(`DELETE FROM user_courses WHERE user_courses.user_id=? and user_courses.course_id=?`, u, c)
	if err != nil {
		log.Info("Failed to delete user from course")
		return err
	}
	return nil
}

func GetUserCourses(uID string) ([]*course.Course, error) {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return nil, err
	}
	rows, err1 := db.Query(`SELECT course_id FROM user_courses WHERE user_id = ?`, uID)
	if err1 != nil {
		log.Info("Failed to get user's courses")
		return nil, err1
	}
	var cA []int
	for rows.Next() {
		var c int
		err = rows.Scan(&c)
		if err != nil {
			log.Info("Failed to scan rows from DB")
		}
		cA = append(cA, c)
	}
	rows.Close()
	var courses []*course.Course
	for _, i := range cA {
		c, err3 := course.GetByID(strconv.Itoa(i))
		if err3 != nil {
			log.Info("failed to find Courses in DB")
			return nil, err3
		}
		courses = append(courses, c)
	}
	return courses, nil
}

func DeleteUser(uID string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err = db.Exec(`DELETE FROM users WHERE id=?`, uID)
	if err != nil {
		log.Info("Failed to delete user")
		return err
	}
	_, err = db.Exec(`DELETE FROM user_courses WHERE user_id=?`, uID)
	if err != nil {
		log.Info("Failed to delete user's courses")
		return err
	}
	return nil
}
