package lesson

import (
	"context"
	"creator/cache"
	"creator/databaseSQL"
	"creator/log"
	"creator/service/translate"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"strconv"
	"time"
)

// LESSON SERVICES
func Create(ln string, ld string) (int64, error) {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return 0, err
	}
	nameEn := translate.Convert(ln)
	descEn := translate.Convert(ld)
	result, err1 := db.Exec(`INSERT INTO lessons (name, description, name_en, description_en ) VALUES (?,?,?,?)`, ln, ld, nameEn, descEn)
	//result, err1 := db.Exec(`INSERT INTO lessons (name, description) VALUES (?,?)`, ln, ld)
	if err1 != nil {
		log.Info("Failed to insert values into lessons")
		return 0, err1
	}
	id, err3 := result.LastInsertId()
	if err3 != nil {
		log.Info("Failed to get inserted value's ID")
		return 0, err3
	}
	return id, nil
}

func GetByID(lID string) (*Lesson, error) {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return nil, err
	}
	l := &Lesson{}
	err = db.QueryRow(`SELECT id, name,description FROM lessons WHERE lessons.id = ?`, lID).Scan(&l.ID, &l.Name, &l.Description)
	if err != nil {
		log.Info("Failed to get lesson by ID")
		return nil, err
	}
	return l, nil
}

func Delete(lID string) error {
	db, err := databaseSQL.GetInstance()
	if err != nil {
		log.Info("SQL DB Connection Failed")
		return err
	}
	_, err = db.Exec(`DELETE FROM lessons WHERE id = ? `, lID)
	if err != nil {
		log.Info("Failed to delete lesson from lessons")
		return err
	}
	_, err = db.Exec(`DELETE FROM course_lessons WHERE lesson_id = ? `, lID)
	if err != nil {
		log.Info("Failed to delete lesson from course_lessons")
		return err
	}
	err1 := DeleteFromCache(lID)
	if err1 != nil {
		log.Info("Failed to delete lesson from cache")
		return err
	}
	return nil
}

// cache lessons
func CreateToCache(l *Lesson, cID string) error {
	var ctx = context.Background()
	client := cache.GetInstance()
	bytes, _ := json.Marshal(l)
	err := client.Set(ctx, `lesson:`+strconv.FormatInt(l.ID, 10), bytes, 120*time.Second).Err()
	if err != nil {
		return err
	}
	return nil
}

func DeleteFromCache(lID string) error {
	var ctx = context.Background()
	client := cache.GetInstance()
	_, err := client.Get(ctx, `lesson:`+lID).Result()
	if err == redis.Nil {
		return nil
	} else {
		err := client.Del(ctx, `lesson:`+lID).Err()
		if err != nil {
			return err
		}
		return nil
	}
}
