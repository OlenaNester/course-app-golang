package lesson

type Lesson struct {
	ID          int64  `json:"lesson_id"`
	Name        string `json:"lesson_name"`
	Description string `json:"lesson_description"`
}
