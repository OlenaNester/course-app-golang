package controllers

import (
	"creator/entities/course"
	"creator/responses"
	"github.com/gorilla/mux"
	"net/http"
)

type CourseController struct {
	courses []*course.Course
	router  *mux.Router
}

func (cc *CourseController) RegisterRouter(r *mux.Router) {
	cc.router = r
}

func (cc *CourseController) RegisterActions() {
	// CREATE COURSE
	// localhost:8080/course/create/Golang
	cc.router.HandleFunc("/course/create/{name}", cc.courseCreation).Methods("POST")

	// DELETE COURSE
	// localhost:8080/course/delete/1
	cc.router.HandleFunc("/course/delete/{courseID}", cc.deleteCourse).Methods("DELETE")

	// RENAME COURSE
	// localhost:8080/course/1/rename/C++
	cc.router.HandleFunc("/course/{courseID}/rename/{newname}", cc.courseRename).Methods("PUT")

	//GET COURSE
	//localhost:8080/course/2
	cc.router.HandleFunc("/course/{courseID}", cc.getCourse).Methods("GET")

	//GET COURSES
	//localhost:8080/courses
	cc.router.HandleFunc("/courses", cc.getCourses).Methods("GET")
}

func (cc *CourseController) courseCreation(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var courseName = vars["name"]
	cID, err := course.Create(courseName)
	if err != nil {
		responses.ResponseError(`Failed to create course on DB`, err, rw)
	} else {
		c := &course.Course{ID: cID, Name: courseName}
		err1 := course.CreateCourseCache(c)
		if err1 != nil {
			responses.ResponseError(`Failed to create course on cache`, err1, rw)
		} else {
			responses.ResponseCreate("Course", courseName, rw)
		}
	}
}

func (cc *CourseController) deleteCourse(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var cID = vars["courseID"]
	err := course.Delete(cID)
	if err != nil {
		responses.ResponseError(`Failed to delete course`, err, rw)
	} else {
		responses.ResponseAction("Course", cID, "", "", "deleted", rw)
	}
}

func (cc *CourseController) courseRename(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var cID = vars["courseID"]
	var newName = vars["newname"]
	err := course.Rename(cID, newName)
	if err != nil {
		responses.ResponseError(`Failed to update on DB `, err, rw)
	} else {
		responses.ResponseAction("Course", cID, "New course name", newName, "update", rw)
	}
}

func (cc *CourseController) getCourse(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var cID = vars["courseID"]
	c, err := course.FindValue(cID)
	if err != nil {
		responses.ResponseError(`Failed to find user on DB`, err, rw)
	} else {
		course.ResponseCourse(c, rw)
	}
}

func (cc *CourseController) getCourses(rw http.ResponseWriter, _ *http.Request) {
	c, err := course.GetCourses()
	if err != nil {
		responses.ResponseError(`Failed to find user on DB`, err, rw)
	} else {
		course.ResponseCourses(c, "courses: ", rw)
	}
}
