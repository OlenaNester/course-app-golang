package controllers

import (
	"creator/entities/course"
	"creator/entities/lesson"
	"creator/log"
	"creator/responses"
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"net/http"
	"strconv"
)

type LessonController struct {
	lessons []*lesson.Lesson
	router  *mux.Router
}

func (lc *LessonController) RegisterRouter(r *mux.Router) {
	lc.router = r
}

func (lc *LessonController) RegisterActions() {
	// CREATE LESSON - UPDATE COURSE
	// localhost:8080/lesson/create/course/2
	//	requestBody:
	//		{
	//			"lesson_name": "INTRO",
	//			"lesson_description":"Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi deserunt quidem unde, laudantium quam sequi iste fugit ipsa. Voluptatem, nostrum."
	//		}
	lc.router.HandleFunc("/lesson/create/course/{courseID}", lc.createLessonOnCourse).Methods("PUT")

	// DELETE LESSON
	// localhost:8080/lesson/delete/5
	lc.router.HandleFunc("/lesson/delete/{lessonID}", lc.deleteLesson).Methods("DELETE")

}

func (lc *LessonController) createLessonOnCourse(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	bodyBytes, err := io.ReadAll(r.Body)
	if err != nil {
		log.Info("Error with read request body")
	}
	var l *lesson.Lesson
	json.Unmarshal(bodyBytes, &l)
	defer r.Body.Close()

	var vars = mux.Vars(r)
	var cID = vars["courseID"]
	lID, err2 := lesson.Create(l.Name, l.Description)
	if err2 != nil {
		responses.ResponseError(`Failed to create lesson on DB `, err2, rw)
	}
	err1 := course.Update(cID, strconv.FormatInt(lID, 10))
	if err1 != nil {
		responses.ResponseError(`Failed to update course with lesson on DB `, err2, rw)
	}
	l.ID = lID
	_ = lesson.CreateToCache(l, cID)
	_ = course.AppendLessonCache(cID)
	responses.ResponseAction("Course with ID", cID, "Lesson with ID", strconv.FormatInt(lID, 10), "append", rw)
}

func (lc *LessonController) deleteLesson(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	var vars = mux.Vars(r)
	var lID = vars["lessonID"]
	err := lesson.Delete(lID)
	if err != nil {
		responses.ResponseError(`Failed to create lesson on DB `, err, rw)
	} else {
		responses.ResponseAction("Lesson with ID ", lID, "", "", "deleted", rw)
	}
}
