package controllers

import (
	"creator/entities/course"
	"creator/entities/user"
	"creator/responses"
	"github.com/gorilla/mux"
	"net/http"
)

type UserController struct {
	users  []*user.User
	router *mux.Router
}

func (uc *UserController) RegisterRouter(r *mux.Router) {
	uc.router = r
}

func (uc *UserController) RegisterActions() {
	// CREATE USER
	// localhost:8080/createuser/Fillip/fillip@email.com
	uc.router.HandleFunc("/createuser/{user}/{user_email}", uc.registration).Methods("POST")

	//GET USER INFO
	//localhost:8080/user/{userID}/me
	uc.router.HandleFunc("/user/{userID}/me", uc.getUser).Methods("GET")

	//REGISTRATION USER ON THE COURSE
	// localhost:8080/user/subscribe/1/1
	uc.router.HandleFunc("/user/subscribe/{userID}/{courseID}", uc.subscription).Methods("POST")

	//GET LIST OF USERS COURSES
	//localhost:8080/user/{userID}/courses
	uc.router.HandleFunc("/user/{userID}/courses", uc.getUserCourses).Methods("GET")

	//UNSUBSCRIBE USER FROM THE COURSE
	// localhost:8080/user/unsubscribe/1/1
	uc.router.HandleFunc("/user/unsubscribe/{userID}/{courseID}", uc.unsubscription).Methods("PATCH")

	//DELETE USER ACCOUNT
	// localhost:8080/user/delete/1`
	uc.router.HandleFunc("/user/delete/{userID}", uc.deleteAccount).Methods("DELETE")
}

func (uc *UserController) registration(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var userName = vars["user"]
	var userEmail = vars["user_email"]
	_, err1 := user.CreateUser(userName, userEmail)
	if err1 != nil {
		responses.ResponseError(`Failed to create on DB`, err1, rw)
	} else {
		responses.ResponseCreate("User", userName, rw)
	}
}

func (uc *UserController) getUser(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var uID = vars["userID"]
	u, err := user.FindUser(uID)
	if err != nil {
		responses.ResponseError(`Failed to find user on DB`, err, rw)
	} else {
		user.Response(u, rw)
	}
}

func (uc *UserController) subscription(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var userID = vars["userID"]
	var courseID = vars["courseID"]
	err := user.RegisterUserToCourse(userID, courseID)
	if err != nil {
		responses.ResponseError(`Failed to subscribe user to course `, err, rw)
	} else {
		responses.ResponseAction("User with ID ", userID, "Course with ID: ", courseID, "subscribed", rw)
	}
}

func (uc *UserController) unsubscription(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var userID = vars["userID"]
	var courseID = vars["courseID"]
	err := user.UnsubscribeUserFromCourse(userID, courseID)
	if err != nil {
		responses.ResponseError(`Failed to unsubscribe user from course `, err, rw)
	} else {
		responses.ResponseAction("User with ID: ", userID, "Course with ID: ", courseID, "unsubscribed", rw)
	}
}

func (uc *UserController) getUserCourses(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var uID = vars["userID"]
	courses, err := user.GetUserCourses(uID)
	if err != nil {
		responses.ResponseError(`Failed to find user on DB`, err, rw)
	} else {
		course.ResponseCourses(courses, "user_courses: ", rw)
	}
}

func (uc *UserController) deleteAccount(rw http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var uID = vars["userID"]
	err := user.DeleteUser(uID)
	if err != nil {
		responses.ResponseError(`Failed to delete User from DB`, err, rw)
	} else {
		responses.ResponseAction("User with ID", uID, "", "", "deleted", rw)
	}
}
