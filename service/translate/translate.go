package translate

import (
	"bytes"
	"creator/log"
	"encoding/json"
	"github.com/joho/godotenv"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

type BStruct struct {
	Texts []string `json:"texts"`
	To    []string `json:"to"`
	From  string   `json:"from"`
}

type BodyResponse struct {
	Translations []struct {
		To         string   `json:"to"`
		Translated []string `json:"translated"`
	} `json:"translations"`
	From                  string `json:"from"`
	Translated_characters int    `json:"translated_characters"`
}

func Convert(t string) string {

	err3 := godotenv.Load()
	if err3 != nil {
		log.Info("Error loading .env file")
		return ""
	}
	c := http.Client{Timeout: 10 * time.Second}
	lang := []string{"en"}
	from := "uk"
	var bodyStruct BStruct
	bodyStruct.Texts = []string{t}
	bodyStruct.To = lang
	bodyStruct.From = from
	body, _ := json.Marshal(bodyStruct)

	req, err := http.NewRequest(`POST`, `https://api.lecto.ai/v1/translate/text`, bytes.NewBuffer(body))
	if err != nil {
		log.Info("Fail to send request to translate")
		return "Something wrong with your request"
	}
	req.Header.Add(`Accept`, `application/json`)
	req.Header.Add(`Content-Type`, `application/json`)
	req.Header.Add(`X-API-Key`, os.Getenv("TRANSLATE_KEY"))

	resp, err := c.Do(req)
	if err != nil {
		log.Info("Failed to fetch")
		return "Error with request from REST server to JSON_RPC server"
	}
	defer resp.Body.Close()
	bodyRespons, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Info("Failed to Read resp body")
	}
	var bodyResponse BodyResponse
	json.Unmarshal(bodyRespons, &bodyResponse)
	result := bodyResponse.Translations[0].Translated
	return strings.Join(result, " ")
}
