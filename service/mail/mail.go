package mail

import (
	"creator/config"
	"creator/log"
	"fmt"
	"net/smtp"
)

func SendEmail(c string, e string) error {
	from := config.GetDotEnv().Email
	pass := config.GetDotEnv().Password
	if c == "" {
		log.Info("no course is provided \"\"")
		return fmt.Errorf("no course is provided \"\"")
	}
	if e == "" {
		log.Info("no email is provided \"\"")
		return fmt.Errorf("no email is provided \"\"")
	}
	to := []string{e}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"
	body := `You signed up for the course` + ` ` + c + `. It will start on 29.02.2031`
	subject := "Subject:Greetings on a new course which will change your life! \n"
	message := []byte(subject + body)
	auth := smtp.PlainAuth("", from, pass, smtpHost)
	err1 := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err1 != nil {
		log.Info("Failed to send email with body")
		return err1
	}
	return nil
}
