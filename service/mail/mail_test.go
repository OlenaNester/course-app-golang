package mail

import (
	"testing"
)

func TestSendEmail(t *testing.T) {
	type args struct {
		course string
		email  string
	}
	tests := []struct {
		name       string
		args       args
		wantErr    bool
		wantErrStr string
	}{
		{
			name: "error when no course and no email is provided",
			args: args{
				course: "",
				email:  "superemail@email.com",
			},
			wantErr:    true,
			wantErrStr: "no course is provided \"\"",
		},
		{
			name: "error when course is provided but no email is on set",
			args: args{
				course: "Golang",
				email:  "",
			},
			wantErr:    true,
			wantErrStr: "no email is provided \"\"",
		},
		{
			name: "success when course is provided and email is on set",
			args: args{
				course: "Golang",
				email:  "superemail@email.com",
			},
			wantErr:    false,
			wantErrStr: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := SendEmail(tt.args.course, tt.args.email)
			if err != nil {
				if !tt.wantErr {
					t.Errorf("SendEmail() error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				if err.Error() != tt.wantErrStr {
					t.Errorf("SendEmail error = %v, wantErr %v", err, tt.wantErrStr)
					return
				}
			}

		})
	}
}
